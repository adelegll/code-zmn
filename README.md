# Code ZMN

À la croisée de l’arabe et du français, le code ZMN est un projet d’alphabet phonétique pensé par Rachid Zamani (https://www.codezmn.com/) comme outil pédagogique à destination des arabophones qui apprennent à lire et à écrire le français. Il se dédouble en code ludique qui simplifie radicalement la langue de Molière. Et il devient ici un projet typographique qui bouleverse nos repères langagiers, un projet poétique qui lettre des mixités. 

Premiers fichiers mis en ligne le 2 septembre 2020 à l'occasion de mon jury de fin de Master à la Cambre en typographie. Versions de travail encore en cours.

Adèle Gallé, fonte publiée sous License OFL https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL_web